package MatchData;

import java.sql.*;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.ParseException;
import java.text.SimpleDateFormat; 

public class Match {
	Statement dbconn;   
	
  
	
	public int IdMatch(String p_pin, String p_name, String p_phone, Date dob)
	{
		int v_sid=0;
		String v_pin, v_name, v_phone;
		Date v_dob;
		int mpin = 0;
		int mphone = 0;
		int mname = 0;
		int mdob = 0;
		int isMatched = 0;
		
		String get_data;
		
		get_data = "SELECT sid, name, dob, phone_num, pin "
				+ "FROM s_identical\r\n"
				+ "WHERE pin =" + p_pin +" OR phone_num = "+p_phone+";";
		
		ResultSet rs;
		try {
			rs = dbconn.executeQuery(get_data);
			while(rs.next()) 
				{
					v_sid = rs.getInt(1);
					mname = NameMatch(rs.getString(2),p_name);
					mphone = PhoneMatch(rs.getString(4),p_phone);
					Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(3)); 
					mdob = DobMatch(date1,dob);

					if(rs.getString(5)== p_pin)
						{
							mpin = 2;
						}
				}
			if(mname + mdob + mphone + mpin >= 3)
			{
				isMatched += 1;
				if(isMatched > 1 ){
					return -1;
				}
			}
			if(isMatched == 1)
			return v_sid;
		else if (isMatched == 0)
			return 0;
			//else return -1;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  

		return -2;
		
	}

	public int NoIdMatch(String p_doctype, String p_docnum,String p_name, String p_phone, Date dob ){
		int v_sid=0;
		String v_pin, v_name, v_phone, v_doctype, v_docnum;
		Date v_dob;
		int mpin = 0;
		int mphone = 0;
		int mname = 0;
		int mdob = 0;
		int mdoc =0;
		int isMatched = 0;

		String get_data;

		get_data = "SELECT sid, name, dob, phone_num, doctype, docnum "
		+ "FROM s_identical\r\n"
		+ "WHERE (doctype= "+p_doctype+"AND docnum = "+ p_docnum+") OR phone_num = "+p_phone+";";
		ResultSet rs;
		try {
			rs = dbconn.executeQuery(get_data);

			while(rs.next()) 
				{
					v_sid = rs.getInt(1);
					mname = NameMatch(rs.getString(2),p_name);
					mphone = PhoneMatch(rs.getString(4),p_phone);
					Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(3)); 
					mdob = DobMatch(date1,dob);

					v_doctype= rs.getString(5);
					v_docnum= rs.getString(6);

					if(v_doctype == p_doctype && v_docnum == p_docnum)
						{
							mdoc = 1;
						}
					
				}
				if(mname + mdob + mphone + mdoc >= 3)
			{
				isMatched += 1;
				if(isMatched > 1 ){
					return -1;
				}
				if(isMatched == 1)
					return v_sid;
				else if (isMatched == 0)
					return 0;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -2;
	}

	public int NameMatch(String name1 , String name2){
		String front, v_name1, v_name2;
		int frontlen;
		boolean match_all_1 = true;
		boolean match_all_2 = true;

		if(name1 == null || name2 == null){
			return 0;
		}

		v_name1 = name1.trim().toUpperCase();
		v_name2 = name2.trim().toUpperCase();

		while(v_name1 == null || v_name1.length()==0){
			Pattern pat = Pattern.compile("[A-z]*");
			Matcher mat = pat.matcher(v_name1);
			front = mat.toString();
			frontlen = front.length();

			v_name1 = v_name1.substring(frontlen+1);

			if(v_name2.indexOf(front)==0){
				match_all_1 = false;
				break;
			}
		}
		while(v_name2 == null || v_name2.length()==0){
			Pattern pat = Pattern.compile("[A-z]*");
			Matcher mat = pat.matcher(v_name2);
			front = mat.toString();
			frontlen = front.length();

			v_name2 = v_name2.substring(frontlen+1);

			if(v_name1.indexOf(front)==0){
				match_all_2 = false;
				break;
			}
		}

		if ( match_all_1 && match_all_2){
			return 1;
		}
		return 0;
	}

	public int PhoneMatch(String phone1, String phone2){
		String v_phone1, v_phone2;

		v_phone1= phone1.replaceAll(" ", "");
		v_phone2 = phone2.replaceAll(" ", "");
		
		if(v_phone1.charAt(0)==0){
			v_phone1= "84"+ v_phone1.substring(1);
		}

		if(v_phone2.charAt(0)==0){
			v_phone2= "84"+ v_phone2.substring(1);
		}

		if(v_phone1 == v_phone2){
			return 1;
		}
		return 0;
	}
	public int DobMatch(Date date1, Date date2) {
		boolean ismatched = false;
		int y1,y2,m1,m2,d1,d2;
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date1);
		
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(date2);
		
		y1 = calendar1.get(Calendar.YEAR);
		m1= calendar1.get(Calendar.MONTH);
		d1 = calendar1.get(Calendar.DAY_OF_MONTH);
		
		y2 = calendar2.get(Calendar.YEAR);
		m2= calendar2.get(Calendar.MONTH);
		d2 = calendar2.get(Calendar.DAY_OF_MONTH);
		
		if(date1 == date2 )
			ismatched = true;
		
		if(y1==y2 && m1 == 1 && d1 == 1 )
			ismatched = true;
		
		if(y1==y2 && m2 == 1 && d2 == 1 )
			ismatched = true;
		
		if(ismatched == true)
			return 1;
		
		return 0;
	}
	

	
	}
