package MatchData;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import MatchData.ObjectDataConnect;

public class ConfigImport {

	public static List<String> listFiles = new ArrayList<>();

//	public static String InputCustomerPath() {
//		String path = "";
//		Properties prop = new Properties();
//		String fileName = "src/import.txt";
//		try (FileInputStream fis = new FileInputStream(fileName)) {
//			prop.load(fis);
//		} catch (FileNotFoundException ex) {
//			System.out.println("app.name");
//		} catch (IOException ex) {
//			System.out.println("app.name");
//		}
//
//		path = prop.getProperty("import.customer");
//		return path;
//	}
//
//	public static String InputContractPath() {
//		String path = "";
//		Properties prop = new Properties();
//		String fileName = "src/import.txt";
//		try (FileInputStream fis = new FileInputStream(fileName)) {
//			prop.load(fis);
//		} catch (FileNotFoundException ex) {
//			System.out.println("app.name");
//		} catch (IOException ex) {
//			System.out.println("app.name");
//		}
//
//		path = prop.getProperty("import.contract");
//		return path;
//	}
//
//	public static String InputHistoryContractPath() {
//		String path = "";
//		Properties prop = new Properties();
//		String fileName = "src/import.txt";
//		try (FileInputStream fis = new FileInputStream(fileName)) {
//			prop.load(fis);
//		} catch (FileNotFoundException ex) {
//			System.out.println("app.name");
//		} catch (IOException ex) {
//			System.out.println("app.name");
//		}
//
//		path = prop.getProperty("import.history.contract");
//		return path;
//	}
//
//	public static String InputErrorPath() {
//		String path = "";
//		Properties prop = new Properties();
//		String fileName = "src/import.txt";
//		try (FileInputStream fis = new FileInputStream(fileName)) {
//			prop.load(fis);
//		} catch (FileNotFoundException ex) {
//			System.out.println("app.name");
//		} catch (IOException ex) {
//			System.out.println("app.name");
//		}
//
//		path = prop.getProperty("import.error");
//		return path;
//	}
//	
	public static ObjectDataConnect GetConnectData(){
		ObjectDataConnect ob = new ObjectDataConnect();
		Properties prop = new Properties();
		String fileName = "src/import.txt";
		try (FileInputStream fis = new FileInputStream(fileName)) {
			prop.load(fis);
		} catch (FileNotFoundException ex) {
			System.out.println("app.name");
		} catch (IOException ex) {
			System.out.println("app.name");
		}
		
		String type = prop.getProperty("import.typeDB");
		
		String url = "";
		String dbName = "";
		String driver = "";
		String userName = "";
		String passWord = "";
		String cc = prop.getProperty("import.customer");
		if(type != null) {
			if(type.equals("2")) {
				// là trường hợp kết nối đến DB oracle
				url = prop.getProperty("import.urlOracle");
				dbName = prop.getProperty("import.dbNameOracle");
				driver = prop.getProperty("import.driverOracle");
				userName = prop.getProperty("import.userNameOracle");
				passWord = prop.getProperty("import.passwordOracle");
				
			}else {
				System.out.println("Giá trị là 1");
				url = prop.getProperty("import.url");
				dbName = prop.getProperty("import.dbName");
				driver = prop.getProperty("import.driver");
				userName = prop.getProperty("import.userName");
				passWord = prop.getProperty("import.password");
			}
			
			ob.setUrl(url);
			ob.setDbName(dbName);
			ob.setDriver(driver);
			ob.setUserName(userName);
			ob.setPassWord(passWord);

			return ob;
		}else {
			return null;
		}

	}

}